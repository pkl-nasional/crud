<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class jenisbarang extends Model
{
    use HasFactory;
    protected $table = "jenis_barang";
    protected $primarykey ="id";
    protected $fillable = ['id','jenis_barang'];
}