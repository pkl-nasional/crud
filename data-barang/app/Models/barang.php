<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class barang extends Model
{
    use HasFactory;
    protected $fillable = ['id_jenis_barang', 'nama_barang', 'tanggal_barang', 'gambar_barang'];
    protected $table = 'barang';
    public $timestamps = false;

    public function jenisbarang()
    {
        return $this->belongsTo(jenisbarang::class);
    }
}
