<?php

namespace App\Http\Controllers;

use App\Models\jenisbarang;
use App\Models\barang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class barangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $katakunci = $request->katakunci;
        $baris = 5;
        if (strlen($katakunci)) {
            $data = barang::where('id', 'like', '%$katakunci%')
                ->orWhere('nama_barang', 'like', '%' . $katakunci . '%')
                ->paginate($baris);
        } else {
            $data = barang::orderby('id', 'desc')->paginate($baris);
        }
        barang::with('jenisbarang');
        return view('barang.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenisbarangid = jenisbarang::all();
        return view('barang.create', compact('jenisbarangid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Session::flash('id_jenis_barang', $request->id_jenis_barang);
        // Session::flash('nama_barang', $request->nama_barang);
        // Session::flash('tanggal_barang', $request->tanggal_barang);
        // Session::flash('gambar_barang', $request->gambar_barang);

        $request->validate([
            'id_jenis_barang' => 'required',
            'nama_barang' => 'required',
            'tanggal_barang' => 'required',
            'gambar_barang' => 'required|mimes:png,jpg,jpeg,gif',
        ], [
            'id_jenis_barang.required' => 'Jenis Barang wajib di isi',
            'nama_barang.required' => 'Nama Barang wajib di isi',
            'tanggal_barang.required' => 'Tanggal Barang wajib di isi',
            'gambar_barang.required' => 'silahkan masukan file gambar barang',
            'gambar_barang.mimes' => 'Gambar hanya bisa PNG,JPEG,JPG, Dan JPEG'
        ]);

        $gambar_file = $request->file('gambar_barang');
        $gambar_ekstensi = $gambar_file->extension();
        $gambar_nama = date('ymdhis') . "." . $gambar_ekstensi;
        $gambar_file->move(public_path('gambar'), $gambar_nama);

        $data = [
            'id_jenis_barang' => $request->id_jenis_barang,
            'nama_barang' => $request->nama_barang,
            'tanggal_barang' => $request->tanggal_barang,
            'gambar_barang' => "$gambar_nama",
        ];

        barang::create($data);
        // dd(1);
        return redirect()->route('barang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = barang::find($id);
        $jenis = jenisbarang::all();
        return view('barang.edit', compact('data', 'jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_jenis_barang' => 'required',
            'nama_barang' => 'required',
            'tanggal_barang' => 'required',
        ], [
            'id_jenis_barang.required' => 'Jenis Barang wajib di isi',
            'nama_barang.required' => 'Nama Barang wajib di isi',
            'tanggal_barang.required' => 'Tanggal Barang wajib di isi',
        ]);
        $data = [
            'id_jenis_barang' => $request->id_jenis_barang,
            'nama_barang' => $request->nama_barang,
            'tanggal_barang' => $request->tanggal_barang,
        ];

        if ($request->hasFile('gambar_barang')) {
            $request->validate([
                'gambar_barang' => 'mimes:png,jpg,jpeg,gif',
            ], [
                'gambar_barang.mimes' => 'Gambar hanya bisa PNG,JPEG,JPG, Dan JPEG'
            ]);
            $gambar_file = $request->file('gambar_barang');
            $gambar_ekstensi = $gambar_file->extension();
            $gambar_nama = date('ymdhis') . "." . $gambar_ekstensi;
            $gambar_file->move(public_path('gambar'), $gambar_nama); //SUDAH UPLOAD

            $data_gambar = barang::where('id', $id)->first();
            File::delete(public_path('gambar') . '/' .
                $data_gambar->gambar_barang); //hapus gambar

            $data['gambar_barang'] = $gambar_nama;
        }

        barang::where('id', $id)->update($data);
        return redirect()->to('barang')->with('success', 'Berhasil menambahkan data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = barang::where('id', $id)->first();
        File::delete(public_path('gambar') . '/' . $data->gambar_barang);
        barang::where('id', $id)->delete();
        return redirect()->to('barang')->with('sucsses', 'berhasil melakukan delete data');
    }
}
