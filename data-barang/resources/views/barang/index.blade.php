@extends('template.layouts')
<!-- START DATA -->
@section('konten')
    <div class="my-3 p-3 bg-body rounded shadow-sm">
        <!-- FORM PENCARIAN -->
        <div class="pb-3">
            <form class="d-flex" action="{{ url('barang') }}" method="get">
                <input class="form-control me-1" type="search" name="katakunci" value="{{ Request::get('katakunci') }}"
                    placeholder="Masukkan kata kunci" aria-label="Search">
                <button class="btn btn-secondary" type="submit">Cari</button>
            </form>
        </div>

        <!-- TOMBOL TAMBAH DATA -->
        <div class="pb-3">
            <a href='{{ url('barang/create') }}' class="btn btn-primary">+ Tambah Data</a>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-1">no</th>
                    <th class="col-md-3">Jenis Barang</th>
                    <th class="col-md-2">Nama Barang</th>
                    <th class="col-md-2">Tanggal Masuk</th>
                    <th class="col-md-2">Gambar Barang</th>
                    <th class="col-md-3">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = $data->firstitem(); ?>
                @foreach ($data as $item)
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ $item->id_jenis_barang }}</td>
                        <td>{{ $item->nama_barang }}</td>
                        <td>{{ $item->tanggal_barang }}</td>
                        <td>
                            @if ($item->gambar_barang)
                                <img style="max-width: 50px;
                        max-height: 50px"
                                    src="{{ asset('gambar/' . $item->gambar_barang) }}" />
                            @endif
                        </td>
                        <td>
                            <a href="/barang/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <form class="d-inline" action="{{ url('barang/' . $item->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
        {{ $data->links() }}

        @include('sweetalert::alert')
    </div>
    <!-- AKHIR DATA -->
@endsection
