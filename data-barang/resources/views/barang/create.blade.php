@extends('template.layouts')
<!-- START FORM -->
@section('konten')

@include('sweetalert::alert')
    <form action='{{ url('barang') }}' method='post' enctype="multipart/form-data">
        @csrf
        <div class="my-3 p-3 bg-body rounded shadow-sm">
            <div class="mb-3 row">
                <label for="id_jenis_barang" class="col-sm-2 col-form-label">Jenis barang</label>
                <div class="col-sm-10">
                    <select name="id_jenis_barang" class="form-select" aria-label="Default select example">
                        <option value="">Pilih</option>
                        @foreach ($jenisbarangid as $item)
                            <option value="{{ $item->id }}">{{ $item->jenis_barang }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <a href='{{ url('jenisbarang') }}' class="btn btn-primary"> Tambah Jenis</a>
            <div class="mb-3 row">
                <label for="nama_barang" class="col-sm-2 col-form-label">Nama Barang</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name='nama_barang' id="nama_barang">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="tanggal" class="col-sm-2 col-foem-label">tanggal</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" id="tanggal" name="tanggal_barang">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="gambar_barang" class="col-sm-2 col-form-label">Gambar</label>
                <div class="col-sm-10">
                    <input type="file" class="form-control" name='gambar_barang' id="gambar_barang">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="jurusan" class="col-sm-2 col-form-label"></label>
                <div class="col-sm-10">
                    <button class="btn btn-primary" name="submit" id="simpan" >
                        SIMPAN
                    </button>
                </div>
            </div>
        </div>
        </div>
    </form>
    <!-- AKHIR FORM -->
@endsection
