<?php

use App\Http\Controllers\barangController;
use App\Http\Controllers\jenisbarangController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//tambah jenis barang//
Route::get('/jenisbarang', [jenisbarangController::class,'index']);
Route::get('/jenisbarang/create', [jenisbarangController::class,'create']);
Route::post('/jenisbarang/store', [jenisbarangController::class,'store']);
//Tambah barang//
Route::resource('barang', barangController::class);
